PORTNAME="python"
PORTVERSION="3.13.2"
DEPENDS="\
../../archivers/bzip2
../../archivers/zlib
../../databases/sqlite3
../../databases/gdbm
../../devel/libffi
../../devel/readline
../../security/openssl
../../textproc/expat2"

BUILDPATH="Python-$PORTVERSION"
FETCHURL="https://www.python.org/ftp/python/$PORTVERSION"
FETCHURL="$FETCHURL/Python-$PORTVERSION.tgz"
TESTTARGET="test"
VERSIONURL="https://www.python.org/downloads/source/"
VERSIONREGEX='/Python-\([0-9\.]*\)\.tgz"'

. ../../Share/unixlike.subr

_INSTALLTARGET="install"
for OPTION in $OPTIONS; do
    case "$OPTION" in
        altinstall) _INSTALLTARGET="altinstall" ;;
        lto) _LTO=1 ;;
        lzma) DEPENDS="$DEPENDS
../../archivers/xz" ;;
    esac
done

port_configure() {
    cd "Python-$PORTVERSION"
    set -- --enable-ipv6 --enable-framework="$PREFIX/Library/Frameworks" \
        --enable-optimizations --prefix="$PREFIX" --with-openssl="$PREFIX" \
        --with-system-expat
    if [ "$_LTO" = "1" ]; then
        set -- "$@" --with-lto=full
    fi
    ./configure "$@"
}

port_install() {
    cd "Python-$PORTVERSION"
    # Fix LINKFORSHARED for packages that don't use pkg-config.
    FRAMEWORKS="$PREFIX/Library/Frameworks"
    sed -i "" "/'LINKFORSHARED':/{n;s|'\(.*\)'|'$FRAMEWORKS/\1'|;}" \
        build/lib.macosx-*/_sysconfigdata__darwin_darwin.py
    install -d -v "$PREFIX/Library/Frameworks"
    make "$_INSTALLTARGET"
    "$PREFIX/bin/python${PORTVERSION%.*}" -m pip install \
        --no-warn-script-location --upgrade packaging pip setuptools wheel
}
