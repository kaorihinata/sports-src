# terraform-provider-x
#
# terraform providers are built the same way as single binary go projects,
# but need to be installed in a special hierarchy. terraform-provider-x is
# designed for this purpose.

. ../../Share/go-single.subr

port_install() {
    cd "${BASEPATH:-$PORTNAME-$PORTVERSION}"
    if [ -z "$TFSOURCE" ]
    then
        if [ -z "$FETCHURL" ]
        then
            printf '%s install: error: FETCHURL undefined\n' "${0##*/}" >&2
            return 1
        fi
        # Derive the source attribute (used in actual .tf files) from the
        # FETCHURL. For the time being, this requires a GitHub URL.
        TFSOURCE="${FETCHURL#https://github.com/}"
        TFSOURCE="registry.terraform.io/$TFSOURCE"
        TFSOURCE="${TFSOURCE%/terraform-provider-*}"
        TFSOURCE="$TFSOURCE/${FETCHURL#*/terraform-provider-}"
        TFSOURCE="${TFSOURCE%/archive/*}"
    fi
    DESTDIR="$PREFIX/libexec/terraform/providers/$TFSOURCE"
    case "$(arch)" in
        'arm64') DESTDIR="$DESTDIR/$PORTVERSION/darwin_arm64" ;;
        'i386') DESTDIR="$DESTDIR/$PORTVERSION/darwin_amd64" ;;
    esac
    install -d -m 755 -v "$DESTDIR"
    install -m 644 -v 'LICENSE' 'CHANGELOG.md' 'README.md' "$DESTDIR"
    install -m 755 -v "../tmp_gopath/bin/$PORTNAME" \
        "$DESTDIR/${PORTNAME}_v$PORTVERSION"
    cat <<EOF

  terraform does not search for providers in any systemwide locations. In
  order for terraform to find providers installed by sports, create a
  filesystem_mirror section within the provider_installation section of your
  .terraformrc (located at ~/.terraformrc), and make sure that you also
  include direct section with an exclude for the library, or else all other
  automatic installations will fail::

  provider_installation {
    filesystem_mirror {
      path    = "$PREFIX/libexec/terraform/providers"
      include = ["$TFSOURCE"]
    }
    direct {
      exclude = ["$TFSOURCE"]
    }
  }

EOF
}
