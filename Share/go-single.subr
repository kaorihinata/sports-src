# go-single
#
# A considerable number of go ports are built the same way, and install a
# single binary. go-single provides for these situations.

. '../../Share/unixlike.subr'
unset -f port_configure port_test

port_build() {
    mkdir -p tmp_gocache tmp_gopath
    export GOCACHE="$(pwd)/tmp_gocache" \
           GOPATH="$(pwd)/tmp_gopath" \
           HOME="$(cd ~;pwd)"
    cd "${BUILDPATH:-$PORTNAME-$PORTVERSION}"
    set -- -ldflags="${GOLDFLAGS:--s -w}"
    test -z "$GOSRC" || set -- "$@" "$GOSRC"
    go install "$@"
}

port_clean() {
    # Why do Go packages so often come with write permissions removed when
    # you could just generate the tarball without permissions in the first
    # place?
    test ! -d "${BUILDPATH:-$PORTNAME-$PORTVERSION}" || \
        chmod -R u+w "${BUILDPATH:-$PORTNAME-$PORTVERSION}"
    test ! -d tmp_gopath/pkg || chmod -R u+w tmp_gopath/pkg
}

port_install() {
    install -d -v "$PREFIX/bin"
    for i in tmp_gopath/bin/*; do
        install -v "$i" "$PREFIX/bin/${i##*/}"
    done
}
