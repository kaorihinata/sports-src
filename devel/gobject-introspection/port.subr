PORTNAME="gobject-introspection"
PORTVERSION="1.83.2"
DEPENDS="\
../bison
../glib20
../pkg-config"

FETCHURL="https://download.gnome.org/sources/gobject-introspection"
FETCHURL="$FETCHURL/${PORTVERSION%.*}"
FETCHURL="$FETCHURL/gobject-introspection-$PORTVERSION.tar.xz"
VERSIONURL="${FETCHURL%/*/*}/cache.json"
VERSIONREGEX="LATEST-IS-\([0-9\.]*\)"

. ../../Share/unixlike.subr

port_patch() {
    cd "gobject-introspection-$PORTVERSION"
    patch girepository/gitypelib.c <<EOF
@@ -2263,4 +2263,5 @@
   GModule *m;
 
+  g_irepository_prepend_library_path("$PREFIX/lib");
 #ifdef __APPLE__
   /* On macOS, @-prefixed shlib paths (@rpath, @executable_path, @loader_path)
EOF
}

port_configure() {
    cd "gobject-introspection-$PORTVERSION"
    meson setup _build --prefix="$PREFIX"
}

port_build() {
    cd "gobject-introspection-$PORTVERSION"
    meson compile -C _build
}

port_install() {
    cd "gobject-introspection-$PORTVERSION"
    meson install -C _build
    cat >&2 <<EOF

  WARNING: As of GLib 2.79.0 and gobject-introspection 1.79.0 there is a
  circular dependency between GLib and gobject-introspection. After you've
  installed gobject-introspection, you *must* reinstall GLib in order for
  the new build to pick up gobject-introspection.

EOF
}

port_test() {
    cd "gobject-introspection-$PORTVERSION"
    meson test -C _build
}
