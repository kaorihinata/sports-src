PORTNAME="redis"
PORTVERSION="7.4.2"
DEPENDS="\
../../devel/pkg-config
../../security/openssl"

FETCHURL="https://download.redis.io/releases/redis-$PORTVERSION.tar.gz"
VERSIONURL="${FETCHURL%redis-*}"
VERSIONREGEX='"redis-\([0-9\.]*\).tar.gz">redis-\1.tar.gz<'

. ../../Share/unixlike.subr
unset -f port_configure

port_patch() {
    cd "redis-$PORTVERSION"
    patch src/config.h <<EOF
@@ -31,6 +31,7 @@
 #define __CONFIG_H
 
 #ifdef __APPLE__
+#define _DARWIN_C_SOURCE
 #include <fcntl.h> // for fcntl(fd, F_FULLFSYNC)
 #include <AvailabilityMacros.h>
 #endif
EOF
}

port_build() {
    cd "redis-$PORTVERSION"
    make -j "$LOGICALCPU" BUILD_TLS=yes
}

port_test() {
    cd "redis-$PORTVERSION"
    # Note: Though otherwise unnecessary, some tests require that the
    #       HOME environment variable be set.
    HOME="$(cd ~;pwd)"
    export HOME
    make test BUILD_TLS=yes
}

port_install() {
    cd "redis-$PORTVERSION"
    make PREFIX="$PREFIX" install
}
