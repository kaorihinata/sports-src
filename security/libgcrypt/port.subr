PORTNAME="libgcrypt"
PORTVERSION="1.11.0"
DEPENDS="\
../libgpg-error"

FETCHURL="https://www.gnupg.org/ftp/gcrypt/$PORTNAME"
FETCHURL="$FETCHURL/$PORTNAME-$PORTVERSION.tar.bz2"
VERSIONURL="https://gnupg.org/download/index.html"
VERSIONREGEX='/'"$PORTNAME"'-\([0-9\.]*\)\.tar\.bz2"'

. ../../Share/unixlike.subr

port_patch() {
    cd "libgcrypt-$PORTVERSION"
    # see: https://dev.gnupg.org/T7170
    #      https://dev.gnupg.org/rCbb0895bbb7c6d2b9502cbbf03da14d4ecf27a183
    patch acinclude.m4 <<'EOF'
@@ -92,12 +92,12 @@
     # Now try to grab the symbols.
-    nlist=conftest.nm
-    if AC_TRY_EVAL(NM conftest.$ac_objext \| "$lt_cv_sys_global_symbol_pipe" \> $nlist) && test -s "$nlist"; then
+    ac_nlist=conftest.nm
+    if AC_TRY_EVAL(NM conftest.$ac_objext \| "$lt_cv_sys_global_symbol_pipe" \> $ac_nlist) && test -s "$ac_nlist"; then
       # See whether the symbols have a leading underscore.
-      if $GREP ' _nm_test_func$' "$nlist" >/dev/null; then
+      if $GREP '^. _nm_test_func' "$ac_nlist" >/dev/null; then
         ac_cv_sys_symbol_underscore=yes
       else
-        if $GREP ' nm_test_func$' "$nlist" >/dev/null; then
-          :
+        if $GREP '^. nm_test_func ' "$ac_nlist" >/dev/null; then
+	  :
         else
-          echo "configure: cannot find nm_test_func in $nlist" >&AS_MESSAGE_LOG_FD
+	  echo "configure: cannot find nm_test_func in $ac_nlist" >&AS_MESSAGE_LOG_FD
         fi
EOF
    autoreconf -f -i
}

port_configure() {
    cd "libgcrypt-$PORTVERSION"
    ./configure \
        --enable-static \
        --prefix="$PREFIX" \
        --with-libgpg-error-prefix="$PREFIX"
}
